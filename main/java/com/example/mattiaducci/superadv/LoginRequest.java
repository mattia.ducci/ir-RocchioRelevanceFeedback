package com.example.mattiaducci.superadv;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by mattiaducci on 12/10/17.
 */

public class LoginRequest extends AsyncTask<URL, Void, String> {

    private String username;
    private int age;
    private String sex;
    private Context context;

    public LoginRequest(String _username, int _age, String _sex, Context _context){

        username=_username;
        sex=_sex;
        age=_age;
        context=_context;
    }

    @Override
    protected String doInBackground(URL... params) {

        HttpURLConnection conn = null;
        int ret = 0;
        JSONObject payload = new JSONObject();
        try {
            conn = (HttpURLConnection) params[0].openConnection();
            payload.put("age",age);
            payload.put("sex",sex);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestMethod("POST");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            writer.write(payload.toString());
            writer.close();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            ret = conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(ret);
    }

    @Override
    protected void onPostExecute(String s) {

        switch (s){
            case "0" : // errore
                Log.e("Errore", "0");
                break;
            case "200" : // ok
                //adv http request
                AdvRequest advRequest=new AdvRequest(username,context);
                String request="http://mattia541993.pythonanywhere.com/adv/"+username;
                try {

                    URL url=new URL(request);
                    advRequest.execute(url);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                break;
            case "400" :
                Log.e("Errore","400");
                break;
            case "406" : // not acceptable -> ourPwd sbagliata, quindi per malintenzionati
                Log.e("Errore", ": not acceptable 406");
                break;
            case "500" : // not acceptable -> internal server error
                Log.e("Errore", ": internal server error 500");
                break;
        }
    }
}
