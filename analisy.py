import pymongo
import numpy as np
import pprint
import matplotlib.pyplot as plt

def select_db(client,num_prova):
    if num_prova == 0:
        return client['prova1'] #db locali
    elif num_prova == 1:
        return client['prova2']
    elif num_prova == 2:
        return client['prova3']
    else:
        print 'errore'
        #return client['prova4']

def error(prediction,views):#find estimate error for a certain user

    #prendo da prediction videos che è una lista di stringhe ['3','6']
    video_prediction=prediction['videos'][0:5]#suppongo che l'utente debba apprezzare tutti questi video a meno che non
                                        #siano stati già da lui considerati rilevanti o già visti
    video_prediction=[int(video) for video in video_prediction]
    #prendo da views la video_goodness che è una lista di dict [{'3':1},{...}]
    video_goodness=views['video_goodness']
    #estraggo gli id dei video che sono stati cliccati
    video_clicked=[]
    for doc in video_goodness:
        if doc[doc.keys()[0]] == 1:
            video_clicked.append(doc.keys()[0])

    error=0
    views_category_detail=[]#['dr','dnr',...] categorizzazione di ciò che ha visto l'utente PRIMA della relevance
    dr=[int(video) for video in views['dr']]
    drav=[int(video) for video in views['drav']]
    dnr=[int(video) for video in views['dnr']]

    for index,video in enumerate(video_prediction):#index serve per pesare l'errore
        views_category_detail.append(video_relevance(video,dr,dnr,drav))#per una rappresentaz grafica dell'algoritmo
        if video not in video_clicked:#se l'utente ha giudicato quel video non rilevante
            #controllare che non sia tra quelli già presenti nei suoi rilevanti, perchè il non mi piace (0)
            #può voler significare o che non li vuole rivedere o che li ha già anche rivisti
            if (video not in dr) and (video not in drav):
                error=error+(len(video_goodness)-(index+1))#pesa di più un errore all'inizio che alla fine dei video proposti
    return (error,views_category_detail)


def video_relevance(video,dr,dnr,drav):
    if video in dr:
        return 'dr'
    elif video in dnr:
        return 'dnr'
    elif video in drav:
        return 'drav'
    else:
        return 'new'

#calcolo dell'errore
prove=[]
pp = pprint.PrettyPrinter(indent=4)
db_client=pymongo.MongoClient()

for num_prova in range(0,3):#3 prove: 0,1,2 PARAMETRIZZARE IN BASE A DOVE SIAMO ARRIVATI, LO DEVO SAPERE IO
    db=select_db(db_client,num_prova)
    coll_pred=db['prediction']
    coll_views=db['views']
    coll_user=db['user']
    #trovare tutti gli username
    usernames=[doc['name'] for doc in coll_user.find()]
    experiment_summary={}#sarà contenuto il riassunto di ciascuna prova
    for username in usernames:
        user=coll_user.find_one({'name':username})
        user_summary=[]
        for j in range(0,user['num_iter']):
            views=coll_views.find_one({'num_iter':j,'name':username})
            prediction=coll_pred.find_one({'num_iter':j,'name':username})
            #calcolare l'errore tra la predizione e l'effettivo gradimento (click) dei video mostrati
            (errore,views_detail)=error(prediction,views)
            user_summary.append({'num_iter':j,'error':errore,'views_detail':views_detail})
        experiment_summary[username]=user_summary
    prove.append(experiment_summary)
    db_client.close()#close current connection

#plot dei dati
errors_in_tests=[]
max_iters=[]#maximum number of iteration all over the various tests
max_iter=0

for experiment_summary in prove:
    for k,v in experiment_summary.iteritems():
        for it in v:
            max_iters.append(it['num_iter'])
max_iter=max(max_iters)

#preparo i dati per essere plottati
for experiment_summary in prove:
    users_error_avg_for_each_iteration=[]#i-position in list correspond at the i-th iteration
    num_iter=0

    #per ogni utente prendo il valore d'errore all'iterazione n e ne faccio la media
    for num_iter in range(0,max_iter+1):
        users_error_sum=0
        num_values=0
        for username,summary in experiment_summary.iteritems():
            for elem in summary:
                if elem['num_iter']==num_iter:
                    users_error_sum+=elem['error']
                    num_values+=1
                    break
        if num_values == 0: #nessuno in quella prova è arrivato fino a quella iterazione, l'errore lo assumo costante
                            #a quanto era l'istante prima. simulo il fatto che l'utente abbia fatto più iterazioni "senza migliorare"
            users_error_avg_for_each_iteration.append(users_error_avg_for_each_iteration[-1])
        else:
            users_error_avg_for_each_iteration.append(users_error_sum/num_values)

#    while len(users_error_avg_for_each_iteration)<(max_iter-1):
#        users_error_avg_for_each_iteration.append(0)#padding

    errors_in_tests.append(users_error_avg_for_each_iteration)

#formatting plot
iter_axis=np.linspace(0,max(max_iters),max(max_iters)+1)
plt.xticks(iter_axis)#dico come dev'essere suddiviso l'asse x
plt.ylim(0,15)#15 perchè ho 5 video visti per ogni utente e l'errore max è 5+4+3+2+1=15
plt.title('index of average prediction error across users')
plt.ylabel('prediction error')
plt.xlabel('# iteration')
colors=['b','g','r','c','y']
rocchio_labels=['q=1 dr=0.35 dnr=15 drav=0.5','q=1 dr=0 dnr=1 drav=0','q=1 dr=0.4 dnr=0 drav=0.6']
for k,elem in enumerate(errors_in_tests):
    #come label mettere i vari valori dei parametri di Rocchio che userò
    plt.plot(iter_axis,elem,label=str('test '+str(k+1)+': '+rocchio_labels[k]),color=colors[k])
plt.legend(loc='upper right', frameon=False)
#plt.show()
plt.savefig('results.png')#per salvare il plot su un file chiamato results.png
