# coding=utf-8
import pymongo
import numpy as np
import ast
import random
import sys
#import scipy
from flask import request #access form attribute to obtain payload not in path
from flask import Flask
app = Flask(__name__)

db=pymongo.MongoClient('mongodb://heroku_7p7dplst:4p0afbbrbp7g06iefrjo4mb4nf@ds029446.mlab.com:29446/heroku_7p7dplst',connect=False)['heroku_7p7dplst']
coll_video_inv_index=db['video_inv_index']
coll_video=db['video']
coll_user=db['user']
coll_views=db['views']
coll_user_adv=db['user_adv'] #commodity collection. app interact with this collection to retrieve the videos to see
coll_url_id=db['url-id']#correspondence between url and id (useful for send across network)
coll_prediction=db['prediction']

#parameters of rocchio algo
r_query=1
r_dr=0.4
r_dnr=0.0
r_drav=0.6

num_prova=3

def retrieve_video_by_url(url):
    return coll_video.find_one({'url':url})#find_one() because the url is unic

def retrieve_user_by_name(name):
    return coll_user.find_one({'name':name})

def update_user_profile_on_db(user,new_profile):
    coll_user.update_one({'name':user},{'$set':{'profile':new_profile}})

###################LOGIN############################# NB: SI POSSONO RITORNARE SOLO STRINGHE
@app.route('/login/<username>', methods= ['POST'])
def login(username):
    username=username.lower()
    user=retrieve_user_by_name(username)
    if user is None: #not registered yet
        age=get_agerange_from_age(str(request.json['age']))
        sex=str(request.json['sex'])
        #num_prova lo cambio io manualmente ogni volta che cambio i pesi in rocchio (azzererò in quell'occasion anche num_iter)
        user={'name':username,'age':age,'sex':sex,'profile':[],'dr':[],'dnr':[],'drav':[],'num_iter':0,'num_prova':num_prova}#CAMBIARE NUM_PROVA QUANDO ROCCHIO PARAM CAMBIANO
        coll_user.insert_one(user)

    return str(user)#a string response is automatically converted in a response object with status code 200


def get_agerange_from_age(age):
    if int(age) <= 18:
        return 1
    elif int(age) > 18 and age < 30:
        return 2
    else:
        return 3
################END LOGIN###################

###################SUBMIT#####################
@app.route('/submit/<username>', methods= ['POST'])#user send to rest a string of dict "{'url1':1},{'url2':0}" -> 1 se clicca, 0 altrimenti
def submit(username):
    username=username.lower()
    user=retrieve_user_by_name(username)
    age=user['age']
    sex=user['sex']
    num_iter=user['num_iter']
    num_prova=user['num_prova']
    dr=user['dr']#va bene prenderli qua perchè ho fatto quelle views (che prenderò) avendo questo stato
    dnr=user['dnr']
    drav=user['drav']
    video_goodness=[ast.literal_eval(v) for v in request.json['video_goodness'].split(',')]#result -> array of dict like this [{'url1':1},{'url2':0}]
    doc={'name':username,'age':age,'sex':sex,'num_iter':num_iter,'video_goodness':video_goodness,'num_prova':num_prova,'dr':dr,'dnr':dnr,'drav':drav}
    coll_views.insert_one(doc)#collection useful to compute evaluation statistics
    #update user data
    for video in video_goodness:
        for url in video.keys():
            if video[url]==1:#if this video is relevant for the user, ogni utente avrà come parole nel profilo solo quelle dei dr e dnr
                update_user_profile_with_video(username,url)
    update_dr_dnr_drav_on_db(username,video_goodness)
#    #compute new adv for populate the adv collection (similarity) for the next iteration
    (user_profile_words,weighted_profile)=compute_weighted_user_profile(username,r_query,r_dr,r_dnr,r_drav)#ROCCHIO's here
    All_ids=retrieve_all_docs_available_to_the_user(username)
    #print >> sys.stderr, str(All_ids)
    #All_w=retrieve_weights(user_profile_words,All_ids,[],[])[0]#I pick up only the matrix, video keywords already projected to user keyword space
                                                               #in matrix All_w to find the video id do matrix_row+1
    All_w=project_videos_to_user_space(user_profile_words,All_ids)
    #print >> sys.stderr, 'All_w shape'
    #print >> sys.stderr, str(All_w.shape)
    docs=[]
    for i in range(0,len(All_w)): #for each video
        docs.append(np.array([float(elem) for elem in All_w[i,:]])) #pick a row (weight of words belonging to one video i) and make it an array

    #for video_id,doc in enumerate(docs):
    #    score_video_against_profile=1-scipy.spatial.distance.cosine(weighted_profile,doc)
    #    doc_scores_list.append({'url':video_id+1,'score':score_video_against_profile})
    scores=score(weighted_profile,docs)

    #l'ordine di docs è quello che governa
    doc_scores_list=[]
    for index,s in enumerate(scores):
        doc_scores_list.append({'url':index+1,'score':s})
    to_user=sorted(doc_scores_list, key=lambda x:-x['score'])#decreasing order
    #return str(to_user)
    #for i in range (0,len(All_ids)):
    #    doc_scores_list.append({'url':All_ids[i],'score':scores[i]})
    #to_user=sorted(doc_scores_list, key=lambda x:-x['score'])#decreasing order
#    #update the adv for user collection and the num_iter for the user

    next_step_videos=[]
    for url in to_user:
        next_step_videos.append(url['url'])
    next_user_iter=num_iter+1
    coll_user.update_one({'name':username},{'$set':{'num_iter':next_user_iter}})
    if coll_user_adv.find({'name':username}).count() == 0:#if it's the first time this user see an adv
        coll_user_adv.insert_one({'name':username,'videos':next_step_videos})
    else:
        coll_user_adv.update_one({'name':username},{'$set':{'videos':next_step_videos}})

    #update prediction collection with the predicted relevant video for the next_user_iter-th iteration
    coll_prediction.insert_one({'name':username,'videos':next_step_videos,'num_iter':next_user_iter,'num_prova':num_prova})

    return str(next_step_videos)


#update each time a user click a video
def update_user_profile_with_video(username,url):
    video=retrieve_video_by_url(url)
    user=retrieve_user_by_name(username)
    user_profile=user['profile']
    video_profile=video['keys']
    video_keys=[doc['word'] for doc in video_profile]
    for index,pref in enumerate(user_profile):
        if pref['word'] in video_keys: #if the user has already clicked on a video with the same keyword
            for elem in video_profile:
                if elem['word']==pref['word']:
                    pref['weights'].append(elem['score'])
                    pref['score']=sum(pref['weights'])/len(pref['weights'])#arithmetic mean
                    video_keys.remove(elem['word']) #the keywords remained are those the user has never seen before
    if len(video_keys)!=0:#there are some words that the user has never been clicked before
        for elem in video_profile:
            if elem['word'] in video_keys:#if the keyword is one of those remained
                new_user_pref={'word':elem['word'],
                          'weights':[elem['score']],
                          'score':elem['score']}
                user_profile.append(new_user_pref)
    #update user profile in the db
    update_user_profile_on_db(username,user_profile)

#I call this method when the user has finished to see the videos
def update_dr_dnr_drav_on_db(username,user_choices):#user send to rest a list of dict [{'url1':1},{'url2':0}] -> 1 se clicca, 0 altrimenti
    user=retrieve_user_by_name(username)
    dr,dnr,drav=user['dr'],user['dnr'],user['drav']
    #flags that tell me if the user choices have modified something in document sets
    dr_mod=False
    dnr_mod=False
    drav_mod=False
    for choice in user_choices:
        for url,relevance in choice.items():
            if relevance==1:
                if (url in dr) and (url not in drav) : #if the user has clicked on the same video for the second time
                    drav.append(url)
                    dr.remove(url)
                    drav_mod=True
                    dr_mod=True
                elif url not in dr: #it's the first time that the user click on that video or has changed idea about that video
                    dr.append(url)
                    if url in dnr:
                        dnr.remove(url)#the user has changed idea
                        dnr_mod=True
                    dr_mod=True
                else:
                    pass
            else: #relevance == 0
                if (url not in dnr) and (url not in dr) and (url not in drav):#l'utente può non volere vedere due volte lo stesso video ma non x questo non è più rilevante x lui
                    dnr.append(url)
                    dnr_mod=True
    if dr_mod==True:
        coll_user.update_one({'name':username},{'$set':{'dr':dr}})
    if dnr_mod==True:
        coll_user.update_one({'name':username},{'$set':{'dnr':dnr}})
    if drav_mod==True:
        coll_user.update_one({'name':username},{'$set':{'drav':drav}})


def retrieve_posting(word):
    return coll_video_inv_index.find_one({'word':word})['videos']

def retrieve_posting_urls(posting):
    urls=[]
    for video in posting:
        urls.append(video['url'])
    return urls

def retrieve_posting_score_of(video_url,posting):
    score=0
    for video in posting:
        if video['url']==video_url:
            return video['score']
    return score

def project_videos_to_user_space(user_profile_words,video_ids):
    print >> sys.stderr, str('video_ids')
    print >> sys.stderr, str(video_ids) #stampa su stderr log sul server
    M=np.zeros((80,len(user_profile_words)))
    for i,word in enumerate(user_profile_words):
        posting=retrieve_posting(word)#posting as a list of video associated to word with a score for each video
        posting_videos=retrieve_posting_urls(posting) #video urls only
        print >> sys.stderr, str(word)
        print >> sys.stderr, str(posting_videos) #stampa su stderr log sul server
        for video_id in video_ids:
            if video_id in posting_videos:
                M[int(video_id)-1][i]=retrieve_posting_score_of(video_id,posting)
    return M


def retrieve_weights(words,dr,dnr,drav):#dr,dnr,drav are list of ids, numbers in string format to be converted, mi focalizzo su dr,dnr,drav perchè è il pt di partenza per Rocchio
    dr_w,dnr_w,drav_w=[],[],[] #list of arrays of weights
    index_relevance_table={}#keep the corrispondence between video index and such set among dr, dnr, drav
    videos=set(dr).union(dnr).union(drav)#videos are videos ids
    #print >> sys.stderr, str(videos) #stampa su stderr log sul server
    #M=np.zeros((len(videos),len(words)))
    M=np.zeros((80,len(words)))#numero di video effettivi, l'indice della matrice è l'id del video
    #print >> sys.stderr, str(M.shape)
    for i,word in enumerate(words): #for each word in the user profile
        posting=retrieve_posting(word)#posting as a list of video associated to word with a score for each video
        #print >> sys.stderr, str(posting)
        posting_videos=retrieve_posting_urls(posting) #video urls only
        for video in videos:#videos sono dr+dnr+drav
            if video in posting_videos:#posting_videos sono i video associati a quella parola. se video non è presente x quella dimensione avrà peso 0
                M[int(video)-1][i]=retrieve_posting_score_of(video,posting)#coloumns for words, -1 in rows because I numered videos beginning from 1
                if video in dr and video not in index_relevance_table:
                    index_relevance_table[video]='dr'
                elif video in dnr and video not in index_relevance_table:
                    index_relevance_table[video]='dnr'
                elif video in drav and video not in index_relevance_table:
                    index_relevance_table[video]='drav'
                else:
                    pass

    for k,v in index_relevance_table.items():
        if v=='dr':
            dr_w.append([float(elem) for elem in M[int(k)-1,:]])#-1 perchè i video partono da 1 ma la matrice, va beh, da 0
        elif v=='dnr':
            dnr_w.append([float(elem) for elem in M[int(k)-1,:]])
        else:
            drav_w.append([float(elem) for elem in M[int(k)-1,:]])
    #x debug
    #for i in range(0,80):
    #    print >> sys.stderr, str(M[i,:])

    return (M,np.array(dr_w),np.array(dnr_w),np.array(drav_w)) #videos on rows, words on coloumns

def rocchio(query, crv, cnrv, crav, alpha, beta, gamma, delta):#returns a weighted user profile
    #convert query to array
    q=np.array(query)

    nr = np.zeros(len(query))
    if len(cnrv)>0:
        for v in cnrv:#v is an array
            nr += v
        d = nr * delta / len(cnrv)
    else:
        d=nr

    r = np.zeros(len(query))
    if len(crv)>0:
        for v in crv:
            r += v
        b = r * beta / len(crv)
    else:
        b=r

    rav=np.zeros(len(query))
    if len(crav)>0:
        for v in crav:
            rav+=v
        c=rav*gamma/len(crav)
    else:
        c=rav

    a = alpha * q
    return a + b + c - d

def compute_weighted_user_profile(username,rp_query,rp_dr,rp_dnr,rp_drav):#rp means rocchio param
    user=retrieve_user_by_name(username)
    user_profile=user['profile']
    user_profile_words=[word['word'] for word in user_profile]
    user_profile_scores=[word['score'] for word in user_profile]
    #retrieve relevant, non relevant and already viewed relevant documents
    dr,dnr,drav=user['dr'],user['dnr'],user['drav']#id's
    #project these documents in the user profile space creating weigth vectors
    (M,dr_w,dnr_w,drav_w)=retrieve_weights(user_profile_words,dr,dnr,drav)
    print >> sys.stderr, 'vecchio profilo:'
    print >> sys.stderr, str(user_profile_scores)
    #compute the new user profile
    new_profile=rocchio(user_profile_scores,dr_w,dnr_w,drav_w,rp_query,rp_dr,rp_drav,rp_dnr)
    print >> sys.stderr, 'nuovo profilo:'
    print >> sys.stderr, str(new_profile)
    return (user_profile_words,new_profile)

def retrieve_all_docs_available_to_the_user(username): #matching sex and age range
    user=retrieve_user_by_name(username)
    user_age_range,user_sex=user['age'],user['sex']
    query={'$and':[{'age':user_age_range},{'sex':{'$in':[user_sex,'mf']}}]}
    proj={'_id':0,'url':1}
    doc_urls=[]
    cur=coll_video.find(query,proj)
    for video in cur:
        doc_urls.append(video['url'])
    return doc_urls

def score(q, docs):
    M = np.array(docs)
    scores = np.zeros(len(docs))
    length = [np.linalg.norm(d)*np.linalg.norm(q) for d in docs]
    for i, k in enumerate(q):
        posting = M.T[i]#trasposta per poter fare la moltiplicazione tra vettori (riga x colonna)
        for doc, w in enumerate(posting):
            scores[doc] += k*w
    for i, l in enumerate(length):
        if l>0.0:
            scores[i] = scores[i] / l
        else:
            scores[i]=0
    return [float(elem) for elem in scores]


###################END SUBMIT##################

###################GET ADV###################
@app.route("/adv/<username>")
def get_adv(username):
    username=username.lower()
    num_video_to_select=5#parametrizzare!!!!! in base a quanti video voglio far vedere all'utente x volta
    if coll_user_adv.find({'name':username}).count() == 0:
        #scelgo a random alcune pubblicità tra quelle disponibili per quell'utente (probab uniforme)
        All_videos=retrieve_all_docs_available_to_the_user(username)
        if len(All_videos)>num_video_to_select:
            to_user=random.sample(All_videos,num_video_to_select)#ritorna una lista...
            coll_prediction.insert_one({'name':username,'videos':to_user,'num_iter':0,'num_prova':num_prova})
            return str(to_user)
        else:
            coll_prediction.insert_one({'name':username,'videos':All_videos,'num_iter':0,'num_prova':num_prova})
            return str(All_videos)
    else:
        return str(coll_user_adv.find_one({'name':username})['videos'][0:num_video_to_select])


###################END GET ADV###############




@app.route("/")
def hello():
    return "Hello World!"

if __name__ == '__main__':
    app.run()
