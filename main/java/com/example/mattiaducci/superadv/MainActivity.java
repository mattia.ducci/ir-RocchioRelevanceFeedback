package com.example.mattiaducci.superadv;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private String username;
    private int age;
    private String sex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button loginBtn=(Button)findViewById(R.id.btnLogin);
        final EditText editTextNome=(EditText)findViewById(R.id.editTextNome);
        final EditText editTextAge=(EditText)findViewById(R.id.editTextEta);
        final EditText editTextSesso=(EditText)findViewById(R.id.editTextSesso);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //login REST call
                username=editTextNome.getText().toString();

                if(!editTextAge.getText().toString().isEmpty()) age=Integer.valueOf(editTextAge.getText().toString());
                if(!editTextSesso.getText().toString().isEmpty()) {

                    sex=editTextSesso.getText().toString();
                    Log.e("sex",sex);
                }

                if(!username.equalsIgnoreCase("")){

                    LoginRequest loginRequest=new LoginRequest(username,age,sex,getApplicationContext());
                    String request="http://mattia541993.pythonanywhere.com/login/"+username;
                    try {

                        URL url=new URL(request);
                        loginRequest.execute(url);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }
}
