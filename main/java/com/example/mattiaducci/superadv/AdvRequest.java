package com.example.mattiaducci.superadv;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by mattiaducci on 12/10/17.
 */

public class AdvRequest extends AsyncTask<URL, Void, String> {

    private String username;
    private Context context;

    public AdvRequest(String _username, Context _context){

        username=_username;
        context=_context;
    }

    @Override
    protected String doInBackground(URL... params) {

        //rest call
        HttpURLConnection conn = null;
        String contenuto="";
        int ret = 0;
        try {
            conn = (HttpURLConnection) params[0].openConnection();
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestMethod("GET");

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ret = conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ret = conn.getResponseCode();
            if(ret == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    contenuto += inputLine;
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String returnValue = String.valueOf(ret)+"_"+contenuto;
        return returnValue;
    }

    @Override
    protected void onPostExecute(String s) { //s è ciò che viene ritornato da doInBackground

        String[] split=s.split("_");
        String res=split[0];
        String contenuto=split[1];
        //switcho sulla risposta della richiesta
        switch (res){
            case "0" : // errore
                Log.e("Errore", "0");
                break;
            case "200" : // ok
                Log.e("ADV",s);
                Intent intentOk = new Intent(context, VideoActivity.class);
                intentOk.putExtra("username",username);
                intentOk.putExtra("videoTitles",contenuto);
                context.startActivity(intentOk);
                break;
            case "400" :
                Log.e("Errore","400");
                break;
            case "406" : // not acceptable -> ourPwd sbagliata, quindi per malintenzionati
                Log.e("Errore", ": not acceptable 406");
                break;
            case "500" : // not acceptable -> internal server error
                Log.e("Errore", ": internal server error 500");
                break;
        }


    }
}
