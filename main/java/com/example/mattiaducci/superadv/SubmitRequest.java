package com.example.mattiaducci.superadv;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

/**
 * Created by mattiaducci on 16/10/17.
 */

public class SubmitRequest extends AsyncTask<URL,Void,String> {

    Map<String,Integer> toSubmit;
    Context context;

    public SubmitRequest(Map<String,Integer> _toSubmit, Context _context){

        toSubmit=_toSubmit;
        context=_context;
    }

    @Override
    protected String doInBackground(URL... params) {

        HttpURLConnection conn = null;
        int ret = 0;

        StringBuffer toSend=new StringBuffer();

        for (Map.Entry<String, Integer> entry : toSubmit.entrySet())
        {
            JSONObject jsonObject=new JSONObject();
            try {

                int id=IdUrl.getInstance().getIdFromUrl(entry.getKey());
                jsonObject.put(Integer.toString(id),entry.getValue());
                toSend.append(jsonObject.toString()+",");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

         String stringToSend=toSend.substring(0,toSend.length()-1); //rimuovere l'ultima virgola
        JSONObject jsonObject=new JSONObject();
        try {

            jsonObject.put("video_goodness",stringToSend);

        } catch (JSONException e) {
                e.printStackTrace();
        }
        try {
            conn = (HttpURLConnection) params[0].openConnection();
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestMethod("POST");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            Log.e("send",jsonObject.toString());
            writer.write(jsonObject.toString());
            writer.close();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ret = conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.valueOf(ret);
    }

    @Override
    protected void onPostExecute(String s) {

        switch (s){
            case "0" : // errore
                Log.e("Errore", "0");
                break;
            case "200" : // ok

                Toast.makeText(context, "Grazie :)", Toast.LENGTH_LONG).show();
                //back to login
                Intent i=new Intent(context,MainActivity.class);
                context.startActivity(i);

                break;
            case "400" :
                Log.e("Errore","400");
                break;
            case "406" : // not acceptable -> ourPwd sbagliata, quindi per malintenzionati
                Log.e("Errore", ": not acceptable 406");
                break;
            case "500" : // not acceptable -> internal server error
                Log.e("Errore", ": internal server error 500");
                break;
        }
    }

}
