package com.example.mattiaducci.superadv;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mattiaducci on 17/10/17.
 */

class IdUrl {
    private static final IdUrl ourInstance = new IdUrl();

    static IdUrl getInstance() {
        return ourInstance;
    }
    private static Map<Integer,String> idUrl;

    private IdUrl() {

        idUrl=new HashMap<>();
        idUrl.put(1,"http://irproject.000webhostapp.com/pubblicita/BenvenutiDaU2SUPERMERCATO.mp4");
        idUrl.put(2,"http://irproject.000webhostapp.com/pubblicita/BirraCoronaSpot2017.mp4");
        idUrl.put(3,"http://irproject.000webhostapp.com/pubblicita/BuondiMottaAsteroideSpot2017.mp4");
        idUrl.put(4,"http://irproject.000webhostapp.com/pubblicita/CocaColaBambiniCheCantanoCanzonepubblicitaCocaColaLuglio2013Bimbi.mp4");
        idUrl.put(5,"http://irproject.000webhostapp.com/pubblicita/CornettoAlgidaSenzaGlutineVeganoSpot2017.mp4");
        idUrl.put(6,"http://irproject.000webhostapp.com/pubblicita/FerrarelleViviEffervescenteSpotTV2015.mp4");
        idUrl.put(7,"http://irproject.000webhostapp.com/pubblicita/HuaweiP10P10PlusSpot2017.mp4");
        idUrl.put(8,"http://irproject.000webhostapp.com/pubblicita/IchnusaAnimaSardaSpotNazionale2017.mp4");
        idUrl.put(9,"http://irproject.000webhostapp.com/pubblicita/LaMagiaDelNataleSpotBauli2015.mp4");
        idUrl.put(10,"http://irproject.000webhostapp.com/pubblicita/LaRealtaPerLeNuoveGenerazioniSecondoSamsungSpotTV.mp4");
        idUrl.put(11,"http://irproject.000webhostapp.com/pubblicita/MulinoBiancoSpot2012Tarallucci60s.mp4");
        idUrl.put(12,"http://irproject.000webhostapp.com/pubblicita/PastaVoielloAntoninoCannavacciuoloSpot2017.mp4");
        idUrl.put(13,"http://irproject.000webhostapp.com/pubblicita/pubblicita2010NuovaAlfaRomeoMitoBlackLineConBobSinclar.mp4");
        idUrl.put(14,"http://irproject.000webhostapp.com/pubblicita/SpotNespressoWithGeorgeClooneyAndJeanDujardin.mp4");
        idUrl.put(15,"http://irproject.000webhostapp.com/pubblicita/SpotTIMConTIMBelloAvereTutto.mp4");
        idUrl.put(16,"http://irproject.000webhostapp.com/pubblicita/SpotTreTheFutureYouWant.mp4");
        idUrl.put(17,"http://irproject.000webhostapp.com/pubblicita/StroiliSpotSanValentino2017SilverMoments.mp4");
        idUrl.put(18,"http://irproject.000webhostapp.com/pubblicita/TAGHeuerAtGoldsmithsDontCrackUnderPressure.mp4");
        idUrl.put(19,"http://irproject.000webhostapp.com/pubblicita/tezenisSpot2016RitaOra.mp4");
        idUrl.put(20,"http://irproject.000webhostapp.com/pubblicita/VecchioAmarodelCapoSpot2017.mp4");
        idUrl.put(21,"http://irproject.000webhostapp.com/pubblicita/BurgerKingBaconKingSpot2017.mp4");
        idUrl.put(22,"http://irproject.000webhostapp.com/pubblicita/CarpisaAndPenelopeForTheNewCommercial.mp4");
        idUrl.put(23,"http://irproject.000webhostapp.com/pubblicita/CrodinoTwistAperitivoBiondoSpot2015.mp4");
        idUrl.put(24,"http://irproject.000webhostapp.com/pubblicita/KENZOWorldTheNewFragrance.mp4");
        idUrl.put(25,"http://irproject.000webhostapp.com/pubblicita/LavazzaMaurizioCrozzaInParadisoConUnNuovoPersonaggioEIGrandiClassiciInCapsula.mp4");
        idUrl.put(26,"http://irproject.000webhostapp.com/pubblicita/LevissimaStoriediEverydayClimbersNicolas.mp4");
        idUrl.put(27,"http://irproject.000webhostapp.com/pubblicita/LINESLaTuaNuovaIdeaDiLiberta.mp4");
        idUrl.put(28,"http://irproject.000webhostapp.com/pubblicita/LouisVuittonRollingLuggageCollection.mp4");
        idUrl.put(29,"http://irproject.000webhostapp.com/pubblicita/McDonaldBigMacSiFaIn3.mp4");
        idUrl.put(30,"http://irproject.000webhostapp.com/pubblicita/MorellatoSpotTVSpringSummer2017.mp4");
        idUrl.put(31,"http://irproject.000webhostapp.com/pubblicita/PoisonGirlDiorProfumoDonnaSpot2016.mp4");
        idUrl.put(32,"http://irproject.000webhostapp.com/pubblicita/pubblicitaBrioschiColCinghialeOink.mp4");
        idUrl.put(33,"http://irproject.000webhostapp.com/pubblicita/PubblicitaEviaBaby.mp4");
        idUrl.put(34,"http://irproject.000webhostapp.com/pubblicita/SchweppesAcquaTonicaSpot2017.mp4");
        idUrl.put(35,"http://irproject.000webhostapp.com/pubblicita/Spring20KikoMilano.mp4");
        idUrl.put(36,"http://irproject.000webhostapp.com/pubblicita/THEFRAGRANCEGABRIELLECHANELTHEFILMTHE30VERSION.mp4");
        idUrl.put(37,"http://irproject.000webhostapp.com/pubblicita/TheOneDolce.mp4");
        idUrl.put(38,"http://irproject.000webhostapp.com/pubblicita/UNIQLOSpotLucaThaesler.mp4");
        idUrl.put(39,"http://irproject.000webhostapp.com/pubblicita/VIVILANUOVAESPERIENZAALITALIA.mp4");
        idUrl.put(40,"http://irproject.000webhostapp.com/pubblicita/ZaraWomanCampaignSpringSummer2016.mp4");
        idUrl.put(41,"http://irproject.000webhostapp.com/pubblicita/BalmainxH.mp4");
        idUrl.put(42,"http://irproject.000webhostapp.com/pubblicita/BarillaSpot2017BluBox30.mp4");
        idUrl.put(43,"http://irproject.000webhostapp.com/pubblicita/CanzonePubblicitaLatteGranarolo100VegetaleMarzo2015.mp4");
        idUrl.put(44,"http://irproject.000webhostapp.com/pubblicita/CanzonePubblicitaMuller2016Modelle.mp4");
        idUrl.put(45,"http://irproject.000webhostapp.com/pubblicita/CanzonePubblicitaNastroAzzurroTiPortaLontanoAgosto2015.mp4");
        idUrl.put(46,"http://irproject.000webhostapp.com/pubblicita/CanzonePubblicitaPandoraNatale2015.mp4");
        idUrl.put(47,"http://irproject.000webhostapp.com/pubblicita/CanzonePubblicitaSephoraOttobre2015.mp4");
        idUrl.put(48,"http://irproject.000webhostapp.com/pubblicita/CanzonePubblicitaVolvoV60CrossCountrySettembre2015.mp4");
        idUrl.put(49,"http://irproject.000webhostapp.com/pubblicita/iPhone7TheRockSiriDominateTheDayApple.mp4");
        idUrl.put(50,"http://irproject.000webhostapp.com/pubblicita/LaPubblicitàPiuBellaDiMaggioGiugno2016.mp4");
        idUrl.put(51,"http://irproject.000webhostapp.com/pubblicita/NutellaFerreroSpot2016Da70AnniLaQualitaPrimaDiTutto.mp4");
        idUrl.put(52,"http://irproject.000webhostapp.com/pubblicita/OreoSpotItaliaTuffatiNellaSuaCrema20.mp4");
        idUrl.put(53,"http://irproject.000webhostapp.com/pubblicita/PittaRossoSinginInTheRainConSimonaVenturaHoStonatoSPOT2015.mp4");
        idUrl.put(54,"http://irproject.000webhostapp.com/pubblicita/PubblicitaPrimeDay2016PromozioniPerditaDocchio12Luglio.mp4");
        idUrl.put(55,"http://irproject.000webhostapp.com/pubblicita/SpotEsselungaGiocaVinciNatale2016.mp4");
        idUrl.put(56,"http://irproject.000webhostapp.com/pubblicita/SpotScavolini2016.mp4");
        idUrl.put(57,"http://irproject.000webhostapp.com/pubblicita/SpotWMACelebration.mp4");
        idUrl.put(58,"http://irproject.000webhostapp.com/pubblicita/TicTacNuovoSpot201530.mp4");
        idUrl.put(59,"http://irproject.000webhostapp.com/pubblicita/TucCrackerPossibilandiaCaprettaMotoRapSpot2016.mp4");
        idUrl.put(60,"http://irproject.000webhostapp.com/pubblicita/VersaceErosTheNewFragranceForMen.mp4");
        idUrl.put(61,"http://irproject.000webhostapp.com/pubblicita/2017YamahaYZFR6.mp4");
        idUrl.put(62,"http://irproject.000webhostapp.com/pubblicita/AnnaTatangeloSpotKissimoBiancaluna2017.mp4");
        idUrl.put(63,"http://irproject.000webhostapp.com/pubblicita/BeatsByDre_ConorMcGregor_Dedicated.mp4");
        idUrl.put(64,"http://irproject.000webhostapp.com/pubblicita/ColgateMaxWhiteSpot2017.mp4");
        idUrl.put(65,"http://irproject.000webhostapp.com/pubblicita/DISCOVER_KIDULT_SPOT.mp4");
        idUrl.put(66,"http://irproject.000webhostapp.com/pubblicita/ECOMMERCESpotTv2015Arcaplanet.mp4");
        idUrl.put(67,"http://irproject.000webhostapp.com/pubblicita/eBayeBaySpot2017InfinitiModiDiEssereUnici.mp4");
        idUrl.put(68,"http://irproject.000webhostapp.com/pubblicita/EurosportHomeOfTheOlympicsSpotITA.mp4");
        idUrl.put(69,"http://irproject.000webhostapp.com/pubblicita/GioiaPuraSpotPubblicitarioNovitaGioielliSagapo.mp4");
        idUrl.put(70,"http://irproject.000webhostapp.com/pubblicita/HeareToCreateAdidasSpot2017.mp4");
        idUrl.put(71,"http://irproject.000webhostapp.com/pubblicita/IlNuovoSpotCanonPowerToYourNextStep.mp4");
        idUrl.put(72,"http://irproject.000webhostapp.com/pubblicita/IlNuovoSpotDautoreCirioDiFerzanOzpetek30.mp4");
        idUrl.put(73,"http://irproject.000webhostapp.com/pubblicita/LoSpotCheHaFattoEmozionareIlMondoIntero.mp4");
        idUrl.put(74,"http://irproject.000webhostapp.com/pubblicita/NIVEA_MEN_AfterShave.mp4");
        idUrl.put(75,"http://irproject.000webhostapp.com/pubblicita/PoltroneSofaNataleSpot2017.mp4");
        idUrl.put(76,"http://irproject.000webhostapp.com/pubblicita/ReginaBlitzSpot2017.mp4");
        idUrl.put(77,"http://irproject.000webhostapp.com/pubblicita/SAgapoSpot2013.mp4");
        idUrl.put(78,"http://irproject.000webhostapp.com/pubblicita/SMAKEUPDiCottonPlusSolution2in1.mp4");
        idUrl.put(79,"http://irproject.000webhostapp.com/pubblicita/SpotGoleador2017featFreestyleItalia.mp4");
        idUrl.put(80,"http://irproject.000webhostapp.com/pubblicita/TimDucatiMotoGP.mp4");

    }

    public String getUrlFromId(int id){

        return idUrl.get(id);
    }

    public int getIdFromUrl(String url) {

        for (Map.Entry<Integer, String> entry : idUrl.entrySet()) {

            if(entry.getValue().equalsIgnoreCase(url)) return entry.getKey();
        }

        return -1;
    }
}
