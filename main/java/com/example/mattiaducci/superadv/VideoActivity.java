package com.example.mattiaducci.superadv;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.VideoView;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoActivity extends AppCompatActivity {
    private VideoView videoView = null;
    private ProgressDialog pDialog;
    private String username;
    private Button nextVideoBtn;
    private Button nonMiPiaceBtn;
    private Animation animOpen;
    private Animation animClose;
    private Boolean isVideoClicked;
    private Map<String,Integer> toSubmit;
    private String videoTitles;
    private int videoIndex=0;
    private String[] urls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        // lettura del titolo del video passato nell'intent
        Intent i = getIntent();
        isVideoClicked = false;
        username = i.getStringExtra("username");
        videoTitles=i.getStringExtra("videoTitles");
        //videoTitles="[u'1',u'1']";
        urls=getURLArrayFromString(videoTitles);
        // creazone del dialog con dentro la progress bar di buffering
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Prossima pubblicità");
        pDialog.setMessage("Caricamento...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        // inizializzazione elementi layout
        videoView = (VideoView) findViewById(R.id.vPub_1);
        nextVideoBtn = (Button) findViewById(R.id.btnNext);
        nextVideoBtn.setVisibility(View.GONE);
        nonMiPiaceBtn=(Button)findViewById(R.id.btnNonPiace);
        nonMiPiaceBtn.setVisibility(View.GONE);

        toSubmit=new HashMap<>();

        // inserisco nella videoView il titoli del video preso dall'intent
        //videoView.setVideoURI(Uri.parse(videoTitles));
        videoView.setVideoURI(Uri.parse(urls[0]));

        // metodo che viene attivato quando un video si sta caricando
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // chiudo la schermata di buffering e faccio play del video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                videoView.start();
            }
        });


        // metodo che viene attivato quando ogni video è finito
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                nextVideoBtn.setVisibility(View.VISIBLE);
                nonMiPiaceBtn.setVisibility(View.VISIBLE);

                //if(videoIndex==urls.length-1) nextVideoBtn.setText("Invia risultati");
            }

        });

        //metodo che viene attivato quando l'utente clicca sulla VideoView
        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //toSubmit.put(urls[videoIndex],1);
                nextVideoBtn.setVisibility(View.VISIBLE);
                nonMiPiaceBtn.setVisibility(View.VISIBLE);

                //if(videoIndex==urls.length) nextVideoBtn.setText("Invia risultati");

                Log.e("video","click");
                return true;
            }
        });

        //è il mi piace
        nextVideoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(videoView.isPlaying()) videoView.stopPlayback();//nel caso in cui l'utente clicca sul video prima che sia finito
                toSubmit.put(urls[videoIndex],1);

                //get the next video from the list
                videoIndex+=1;

                if(videoIndex==urls.length) {

                    //set a 0 i video che l'utente non ha cliccato
                    for(int i=0;i<urls.length;i++){

                        if(!toSubmit.containsKey(urls[i])){ //l'utente ha cliccato non mi piace, quindi non ha apportato modifiche alla mappa

                            toSubmit.put(urls[i],0);
                            Log.e("video","non cliccato");
                        }
                    }

                    SubmitRequest submitRequest=new SubmitRequest(toSubmit,getApplicationContext());
                    String request="http://mattia541993.pythonanywhere.com/submit/"+username;

                    try {

                        URL url=new URL(request);
                        submitRequest.execute(url);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                } else{

                    pDialog.show();
                    videoView.setVideoURI(Uri.parse(urls[videoIndex]));
                    videoView.start();
                    nextVideoBtn.setVisibility(View.GONE);
                    nonMiPiaceBtn.setVisibility(View.GONE);
                }

            }
        });

        nonMiPiaceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(videoView.isPlaying()) videoView.stopPlayback();//nel caso in cui l'utente clicca sul video prima che sia finito

                //get the next video from the list
                videoIndex+=1;

                if(videoIndex==urls.length) {

                    //set a 0 i video che l'utente non ha cliccato
                    for(int i=0;i<urls.length;i++){

                        if(!toSubmit.containsKey(urls[i])){ //l'utente ha cliccato non mi piace, quindi non ha apportato modifiche alla mappa

                            toSubmit.put(urls[i],0);
                            Log.e("video","non cliccato");
                        }
                    }

                    SubmitRequest submitRequest=new SubmitRequest(toSubmit,getApplicationContext());
                    String request="http://mattia541993.pythonanywhere.com/submit/"+username;

                    try {

                        URL url=new URL(request);
                        submitRequest.execute(url);

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                } else{

                    pDialog.show();
                    videoView.setVideoURI(Uri.parse(urls[videoIndex]));
                    videoView.start();
                    nextVideoBtn.setVisibility(View.GONE);
                    nonMiPiaceBtn.setVisibility(View.GONE);
                }
            }
        });
    }

/*    private String[] getURLArrayFromString(String s){

        String[] a=s.split(",");
        String patternString="http\\S+mp4";//regex
        Pattern pattern= Pattern.compile(patternString);
        String[] result=new String[a.length];

        for(int i=0;i<a.length;i++){

            Matcher matcher=pattern.matcher(a[i]);

            while(matcher.find()){

                result[i]=matcher.group();
            }
        }

        return result;
    }*/

    private String[] getURLArrayFromString(String s){

        String [] a=s.split(",");
        String patternString="\\d{1,3}";
        Pattern pattern= Pattern.compile(patternString);
        String[] result=new String[a.length];

        for(int i=0;i<a.length;i++){

            Matcher matcher=pattern.matcher(a[i]);

            while(matcher.find()){

                result[i]=matcher.group();
            }
        }

        String[] urlResult=new String[result.length];

        for(int i=0;i<result.length;i++){

            urlResult[i]=IdUrl.getInstance().getUrlFromId(Integer.valueOf(result[i]));
            Log.e("URL",urlResult[i]);
        }
        return urlResult;
    }
}
